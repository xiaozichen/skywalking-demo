package com.ratel.common.entity;

import lombok.Data;

/**
 * @Description
 * @Author fuyongnan
 * @Date 2023/03/11
 * @Version 1.0
 */

@Data
public class GoodsEntity {

    private int goodsId;

    private String goodsName;

    private double goodsPrice;

}
