package com.ratel.order.controller;


import com.ratel.common.entity.OrderEntity;
import com.ratel.order.feign.GoodsFeign;
import com.ratel.order.feign.NoticeFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {


    @Autowired
    GoodsFeign goodsFeign;

    @Autowired
    NoticeFeign noticeFeign;

    @GetMapping("/getOrder")
    public OrderEntity getOrder(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(20230001);
        orderEntity.setUserId("00001");
        orderEntity.setGoods(goodsFeign.getGoods());
        return orderEntity;
    }

    @GetMapping("/sendOrderMsg")
    public String sendOrderMsg(){
        return noticeFeign.sendOrderMsg();
    }

}
