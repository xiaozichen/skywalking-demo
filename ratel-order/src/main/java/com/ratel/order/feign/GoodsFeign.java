package com.ratel.order.feign;

import com.ratel.common.entity.GoodsEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "ratel-goods")
public interface GoodsFeign {

    @GetMapping("/goods/getGood")
    GoodsEntity getGoods();
}
